# Interfaz
Esta pagina esta dedicada al grupo de desarrollo de la interfaz en el proyecto de la SmartBike.



##Funciones##
La interfaz se encarga de realizar la interacción del usuario con la CPU. Atraves de la interfaz se podrá seleccionar el mensaje que se muestre en el POV y tambien se podra visualizar datos importantes tales como la velocidad promedio, la distancia y el estado de la bateria.

### Imágenes ###
En esta sección el cliente podrá visualizar como se llevaría acabo la comunicación con el usuario.

* ![screenshot_00024.jpg](https://bitbucket.org/repo/aqL6Rx/images/3316914597-screenshot_00024.jpg)
Visualización de velocidad.
* ![screenshot_00025.jpg](https://bitbucket.org/repo/aqL6Rx/images/1712243264-screenshot_00025.jpg)
Visualización de bateria.

##Diagrama de flujo
![Diagrama.jpg](https://bitbucket.org/repo/aqL6Rx/images/269072623-Diagrama.jpg)

##Maquina de Estados
![screenshot_00026.jpg](https://bitbucket.org/repo/aqL6Rx/images/3710530872-screenshot_00026.jpg)
## Diagrama
![image.png](https://bitbucket.org/repo/aqL6Rx/images/2544315163-image.png)

## LCD info
Teniendo en cuenta cierta información de esta pagina se estudia la posibilidad de usar un protocolo paralelo de 8-bit ó 4-bit.

* http://www.lxdinc.com/technical_resources/lcd_module_protocol_interfaces#parallel_interface